import torch.nn as nn
from config import ViTConfig

class PatchTokenizer (nn.Module):
    def __init__ (self, config: ViTConfig):
        super (PatchTokenizer, self).__init__ ()

        self.windows = nn.Unfold (kernel_size=config.patch_size, stride=config.patch_size, padding=0)

        # projection for converting into the required dimention using nn.Linear ()
        self.project = nn.Linear ((config.patch_size ** 2), config.tok_len)

    def forward (self, x):
        # transpose for multiplying (a, b) with (b, a)
        x = self.windows (x).transpose (1, 2)
        x = self.project (x)
        return x
