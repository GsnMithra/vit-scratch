from dataclasses import dataclass

@dataclass
class ViTConfig:
    n_head: int
    n_layer: int
    d_model: int
    h_image: int
    w_image: int
    epochs: int
    patch_size: int
    chan_mul: int
    batch_size: int
    def __init__ (self, **kwargs):
        for k, v in kwargs.items ():
            setattr (self, k, v)
            
        self.d_k = self.d_model // self.n_head
        self.tok_len = self.patch_size ** 2

        # add one bacuause of the prediction token
        self.seq_len = (self.h_image // self.patch_size) * (self.w_image // self.patch_size) + 1
