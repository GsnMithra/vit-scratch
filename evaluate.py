import torch
import argparse
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, random_split

from config import ViTConfig
from tools import calculate_accuracy
from vit import ViTModel
from dataset import CatDogDataset


if __name__ == '__main__':
    H, W = (224, 224)

    parser = argparse.ArgumentParser (description='Evaulation of the model')
    parser.add_argument ('--n_head', type=int, default=8, help='Number of heads')
    parser.add_argument ('--n_layer', type=int, default=6, help='Number of Encoder layers')
    parser.add_argument ('--d_model', type=int, default=768, help='Dimention of the embedding table')
    parser.add_argument ('--epochs', type=int, default=35, help='Number of training epochs')
    parser.add_argument ('--patch_size', type=int, default=16, help='Size of a single patch')
    parser.add_argument ('--chan_mul', type=int, default=4, help='Channel multiplier in Feed Forward')
    parser.add_argument ('--batch_size', type=int, default=8, help='Size of the batch')

    parser.add_argument ('--model_path', type=str, required=True, help='Path to the model to eval')
    parser.add_argument ('--dataset_path', type=str, required=True, help='Path to the complete dataset')
   
    parser.add_argument ('--model_evaluation', type=str, required=True, help='Options: both, test, train')

    parser.add_argument ('--device', type=str, default='cpu', help='Model evaluation device')

    args = parser.parse_args ()

    model_path = args.model_path
    config = ViTConfig (
        n_head=args.n_head,
        n_layer=args.n_layer,
        d_model=args.d_model,
        h_image=H,
        w_image=W,
        epochs=args.epochs,
        patch_size=args.patch_size,
        chan_mul=args.chan_mul,
        batch_size=args.batch_size
    )

    device = torch.device (args.device)
    vit = ViTModel (config)
    vit = vit.to (device)

    vit.load_state_dict (
        torch.load (
            model_path,
            map_location=device
        )
    )

    transform = transforms.Compose ([
        transforms.Grayscale (num_output_channels=1),
        transforms.Resize ((H, W)),
        transforms.ToTensor ()
    ])

    dataset = CatDogDataset (args.dataset_path, transform=transform)

    train_size = int (0.8 * len (dataset))
    test_size = len (dataset) - train_size

    train_dataset, test_dataset = random_split (dataset, [train_size, test_size])
    train = DataLoader (train_dataset, batch_size=config.batch_size, shuffle=True)
    test = DataLoader (test_dataset, batch_size=config.batch_size, shuffle=False)

    print ('Evaluating...')

    if args.model_evaluation == 'both':
        train_accuracy = calculate_accuracy (vit, train, device)
        test_accuracy = calculate_accuracy (vit, test, device)
        print (f'Train Accuracy: {train_accuracy}')
        print (f'Test Accuracy: {test_accuracy}')
    elif args.model_evaluation == 'train':
        train_accuracy = calculate_accuracy (vit, train, device)
        print (f'Train Accuracy: {train_accuracy}')
    elif args.model_evaluation == 'test':
        test_accuracy = calculate_accuracy (vit, test, device)
        print (f'Test Accuracy: {test_accuracy}')

