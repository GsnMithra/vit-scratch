import torch

def calculate_accuracy (model, dataloader, device):
    model.eval ()
    correct = 0
    total = 0
    
    with torch.no_grad ():
        for images, labels in dataloader:
            images = images.to (device)
            labels = labels.to (device)

            logits = model (images)
            _, predicted = torch.max (logits, dim=1)
            
            correct += (predicted == labels).sum ().item ()
            total += labels.size (0)
    
    accuracy = correct / total if total > 0 else 0.0
    return accuracy
