import torch
import argparse
from PIL import Image

import torchvision.transforms as transforms
import torch.nn.functional as F

from vit import ViTModel
from config import ViTConfig

if __name__ == "__main__":
    H, W = (224, 224)
    
    parser = argparse.ArgumentParser (description='Model hyperparams, model path and image paths')
    parser.add_argument ('--n_head', type=int, default=8, help='Number of heads')
    parser.add_argument ('--n_layer', type=int, default=6, help='Number of Encoder layers')
    parser.add_argument ('--d_model', type=int, default=768, help='Dimention of the embedding table')
    parser.add_argument ('--epochs', type=int, default=35, help='Number of training epochs')
    parser.add_argument ('--patch_size', type=int, default=16, help='Size of a single patch')
    parser.add_argument ('--chan_mul', type=int, default=4, help='Channel multiplier in Feed Forward')
    parser.add_argument ('--batch_size', type=int, default=8, help='Size of the batch')

    parser.add_argument ('--model_path', type=str, required=True, help='Path to the model')
    parser.add_argument ('--image_path', type=str, required=True, help='Path to the image')

    args = parser.parse_args ()

    model_path = args.model_path 
    image_pred_path = args.image_path 
    
    image = Image.open (image_pred_path)

    transform = transforms.Compose ([
        transforms.Grayscale(num_output_channels=1),
        transforms.Resize((H, W)),
        transforms.ToTensor()
    ])

    img = transform (image)

    config = ViTConfig (
        n_head=args.n_head,
        n_layer=args.n_layer,
        d_model=args.d_model,
        h_image=H,
        w_image=W,
        epochs=args.epochs,
        patch_size=args.patch_size,
        chan_mul=args.chan_mul,
        batch_size=args.batch_size
    )

    vit = ViTModel (config)
    vit.load_state_dict (
        torch.load (
            model_path,
            map_location=torch.device ('cpu')
        )
    )

    label_map = { 0: 'Cat', 1: 'Dog' }

    prediction_label = F.softmax (vit (img.unsqueeze (0)), dim=-1).argmax ()
    prediction_label = prediction_label.detach ().item ()

    print (f'Prediction: {label_map[prediction_label]}') 
