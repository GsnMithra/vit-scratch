# Vision Transformer (ViT) Implementation

This repository contains my custom implementation of the Vision Transformer (ViT) model, which leverages transformer architectures for image classification tasks.

## Table of Contents

- [Overview](#overview)
- [Model Architecture](#model-architecture)
- [Dataset](#dataset)
- [Installation](#installation)
- [Usage](#usage)
- [Results](#results)
- [Contributing](#contributing)
- [License](#license)

## Overview

The Vision Transformer (ViT) is a novel architecture for image classification that replaces traditional convolutional neural networks (CNNs) with a pure transformer model. This repository provides an implementation of ViT along with training and evaluation scripts.

## Model Architecture

The Vision Transformer model divides the input image into fixed-size patches, embeds each patch into a vector, and feeds these vectors into a transformer encoder. The output of the transformer is then used for classification.

## Dataset

You can download the dataset used for training and evaluation from the following link:

[Dataset Download Link](https://www.kaggle.com/datasets/gsnmithra/dog-cat/data)

## Installation

To set up the project, clone the repository and install the necessary dependencies:

```bash
git clone https://gitlab.com/GsnMithra/vit-scratch.git
cd vit-scratch
pip3 install -r requirements.txt
```

## Usage

To train the model, run the following command:

```bash
python3 train.py --dataset_path /path/to/complete/dataset --model_save_name /path/to/save/model
```

The `train.py` script has the following options:

```plaintext
usage: train.py [-h] [--n_head N_HEAD] [--n_layer N_LAYER] [--d_model D_MODEL] [--epochs EPOCHS]
                [--patch_size PATCH_SIZE] [--chan_mul CHAN_MUL] [--batch_size BATCH_SIZE] --dataset_path DATASET_PATH
                [--model_save_name MODEL_SAVE_NAME] [--device DEVICE]

ViT Model Training Info and Hyperparameters

required options:
  --dataset_path DATASET_PATH Path to the complete dataset
  --model_save_name MODEL_SAVE_NAME Name of the model

options:
  -h, --help                  show this help message and exit
  --n_head N_HEAD             Number of heads
  --n_layer N_LAYER           Number of Encoder layers
  --d_model D_MODEL           Dimension of the embedding table
  --epochs EPOCHS             Number of training epochs
  --patch_size PATCH_SIZE     Size of a single patch
  --chan_mul CHAN_MUL         Channel multiplier in Feed Forward
  --batch_size BATCH_SIZE     Size of the batch
  --device DEVICE             Device for PyTorch
```

To evaluate the model, use:

```bash
python3 evaluate.py --model_path /path/to/saved/model --dataset_path /path/to/complete/dataset --model_evaluation both
```

The `evaluate.py` script has the following options:

```plaintext
usage: evaluate.py [-h] [--n_head N_HEAD] [--n_layer N_LAYER] [--d_model D_MODEL] [--epochs EPOCHS]
                   [--patch_size PATCH_SIZE] [--chan_mul CHAN_MUL] [--batch_size BATCH_SIZE] --model_path MODEL_PATH
                   --dataset_path DATASET_PATH --model_evaluation MODEL_EVALUATION [--device DEVICE]

Evaluation of the model

required options:
  --model_path MODEL_PATH     Path to the model to evaluate
  --dataset_path DATASET_PATH Path to the complete dataset
  --model_evaluation MODEL_EVALUATION Options: both, test, train

options:
  -h, --help                  show this help message and exit
  --n_head N_HEAD             Number of heads
  --n_layer N_LAYER           Number of Encoder layers
  --d_model D_MODEL           Dimension of the embedding table
  --epochs EPOCHS             Number of training epochs
  --patch_size PATCH_SIZE     Size of a single patch
  --chan_mul CHAN_MUL         Channel multiplier in Feed Forward
  --batch_size BATCH_SIZE     Size of the batch
  --device DEVICE             Model evaluation device
```

To predict an image, use:

```bash
python3 predict.py --model_path /path/to/saved/model --image_path /path/to/image
```

The `predict.py` script has the following options:

```bash
usage: predict.py [-h] [--n_head N_HEAD] [--n_layer N_LAYER] [--d_model D_MODEL] [--epochs EPOCHS]
                  [--patch_size PATCH_SIZE] [--chan_mul CHAN_MUL] [--batch_size BATCH_SIZE] --model_path MODEL_PATH
                  --image_path IMAGE_PATH

Model hyperparams, model path and image paths

required options:
  --model_path MODEL_PATH
                        Path to the model
  --image_path IMAGE_PATH
                        Path to the image

options:
  -h, --help            show this help message and exit
  --n_head N_HEAD       Number of heads
  --n_layer N_LAYER     Number of Encoder layers
  --d_model D_MODEL     Dimention of the embedding table
  --epochs EPOCHS       Number of training epochs
  --patch_size PATCH_SIZE
                        Size of a single patch
  --chan_mul CHAN_MUL   Channel multiplier in Feed Forward
  --batch_size BATCH_SIZE
                        Size of the batch
```

## Results

- **Training Accuracy**: 78.68%
- **Testing Accuracy**: 77.54%

## Contributing

Contributions are welcome! Please fork the repository and submit a pull request.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
