import sys
import argparse

import torch
import torch.nn as nn
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, random_split

import warnings
warnings.filterwarnings('ignore')

from vit import ViTModel
from config import ViTConfig
from dataset import CatDogDataset
from tools import calculate_accuracy


if __name__ == '__main__':
    H, W = (224, 224)

    parser = argparse.ArgumentParser (description='ViT Model Training Info and Hyperparameters')
    parser.add_argument ('--n_head', type=int, default=8, help='Number of heads')
    parser.add_argument ('--n_layer', type=int, default=6, help='Number of Encoder layers')
    parser.add_argument ('--d_model', type=int, default=768, help='Dimention of the embedding table')
    parser.add_argument ('--epochs', type=int, default=35, help='Number of training epochs')
    parser.add_argument ('--patch_size', type=int, default=16, help='Size of a single patch')
    parser.add_argument ('--chan_mul', type=int, default=4, help='Channel multiplier in Feed Forward')
    parser.add_argument ('--batch_size', type=int, default=8, help='Size of the batch')
    
    parser.add_argument ('--dataset_path', type=str, required=True, help='Path to the complete dataset')
    parser.add_argument ('--model_save_name', type=str, default='models/ViT.pth', help='Name of the model')

    parser.add_argument ('--device', type=str, default='cuda', help='Device for PyTorch')
    args = parser.parse_args ()

    transform = transforms.Compose ([
        transforms.Grayscale (num_output_channels=1),
        transforms.Resize ((H, W)),
        transforms.ToTensor ()
    ])

    dataset = CatDogDataset (args.dataset_path, transform=transform)

    train_size = int (0.8 * len (dataset))
    test_size = len (dataset) - train_size
    train_dataset, test_dataset = random_split (dataset, [train_size, test_size])

    model_save_path = args.model_save_name
    config = ViTConfig (
        n_head=args.n_head,
        n_layer=args.n_layer,
        d_model=args.d_model,
        h_image=H,
        w_image=W,
        epochs=args.epochs,
        patch_size=args.patch_size,
        chan_mul=args.chan_mul,
        batch_size=args.batch_size
    )

    train = DataLoader (train_dataset, batch_size=config.batch_size, shuffle=True)
    test = DataLoader (test_dataset, batch_size=config.batch_size, shuffle=False)
    max_test_accuracy = -sys.maxsize

    device = torch.device (args.device)
    vit = ViTModel (config)
    vit = vit.to (device)

    criterion = nn.CrossEntropyLoss ()
    optimizer = torch.optim.Adagrad (vit.parameters (), lr=0.002)

    print ('Training started...!\n')

    for epoch in range (config.epochs):
        accum_loss = 0.0
        vit.train ()

        for images, labels in train:
            optimizer.zero_grad ()

            images = images.to (device)
            labels = labels.to (device)

            out = vit (images)
            loss = criterion (out, labels)
            loss.backward ()

            accum_loss += loss.item ()

            optimizer.step ()
            
        test_accuracy = calculate_accuracy (vit, test, device)
        max_test_accuracy = max (max_test_accuracy, test_accuracy)
        avg_loss = accum_loss / len (train)
        
        print(f'Epoch ({epoch+1}/{config.epochs}), Loss: ({avg_loss:.4f}) Test Accuracy: ({test_accuracy:.4f})')

    torch.save (vit.state_dict (), model_save_path)
    
    training_accuracy = calculate_accuracy (vit, train, device)

    print ('\nTraining finished...!')
    print (f'Training accuracy: {training_accuracy}, Test accuracy: {max_test_accuracy}')
