import torch
import torch.nn as nn
import torch.nn.functional as F

from config import ViTConfig
from tokenizer import PatchTokenizer
    
class LearnablePositionalEncoder (nn.Module):
    def __init__ (self, config: ViTConfig):
        super (LearnablePositionalEncoder, self).__init__ ()
        self.pos_encoding = nn.Parameter (torch.zeros (1, config.seq_len, config.tok_len), requires_grad=True)

    def forward (self, x):
        out = self.pos_encoding + x
        return out

class Head (nn.Module):
    def __init__ (self, config: ViTConfig):
        super (Head, self).__init__ ()

        self.config = config
        self.query = nn.Linear (config.d_model, config.d_k)
        self.key = nn.Linear (config.d_model, config.d_k)
        self.value = nn.Linear (config.d_model, config.d_k)

        self.linear = nn.Linear (config.d_k, config.d_k)
        
    def forward (self, x):
        Q = self.query (x)
        K = self.key (x)
        V = self.value (x)

        attn = Q @ K.permute (0, 2, 1)
        attn = attn / (self.config.d_k ** (1 / 2))
        attn = F.softmax (attn, dim=-1)
        
        out = (attn @ V)
        out = self.linear (out)

        return out
    
class Attention (nn.Module):
    def __init__ (self, config: ViTConfig):
        super (Attention, self).__init__ ()

        self.heads = nn.ModuleList ([Head (config) for _ in range (config.n_head)])
        self.linear = nn.Linear (config.n_head * config.d_k, config.d_model)
        
    def forward (self, x):
        out = torch.cat ([head (x) for head in self.heads], dim=-1)
        out = self.linear (out)

        return out
    
class FeedForward (nn.Module):
    def __init__ (self, config: ViTConfig):
        super (FeedForward, self).__init__ ()

        self.norm = nn.LayerNorm (config.d_model)
        self.fc1 = nn.Linear (config.d_model, config.d_model * config.chan_mul)
        self.fc2 = nn.Linear (config.d_model * config.chan_mul, config.d_model)
        self.activation = nn.GELU ()

    def forward (self, x):
        x = self.norm (x)
        x = self.fc1 (x)
        x = self.activation (x)
        out = self.fc2 (x)

        return out
    
class Encoder (nn.Module):
    def __init__ (self, config: ViTConfig):
        super (Encoder, self).__init__ ()

        self.norm1 = nn.LayerNorm (config.d_model)
        self.norm2 = nn.LayerNorm (config.d_model)

        self.attention = Attention (config)
        self.feed_forward = FeedForward (config)

    def forward (self, x):
        x = x + self.attention (self.norm1 (x))
        x = x + self.feed_forward (self.norm2 (x))
        return x
    
class ViT (nn.Module):
    def __init__ (self, config: ViTConfig):
        super (ViT, self).__init__ ()

        self.wpe = LearnablePositionalEncoder (config)
        self.cls = nn.Parameter (torch.zeros (1, 1, config.tok_len), requires_grad=True)
        self.project = nn.Linear (config.tok_len, config.d_model)

        self.blocks = nn.Sequential (*[Encoder (config) 
                                       for _ in range (config.n_layer)])

        self.linear = nn.Linear (config.d_model, 2)
        self.norm = nn.LayerNorm (config.d_model)

    def forward (self, x):
        B = x.shape[0]

        x = torch.cat ((self.cls.expand (B, -1, -1), x), dim=1)
        x = self.wpe (x)
        x = self.project (x)

        out = self.blocks (x)
        out = out[:, 0]
        out = self.norm (out)
        logits = self.linear (out)

        return logits
    
class ViTModel (nn.Module):
    def __init__ (self, config: ViTConfig):
        super (ViTModel, self).__init__ ()

        self.patch_tokens = PatchTokenizer (config)
        self.vit = ViT (config)

    def forward (self, x):
        # x (C, H, W)
        x = self.patch_tokens (x) # x (B, T, D)
        logits = self.vit (x)

        return logits
